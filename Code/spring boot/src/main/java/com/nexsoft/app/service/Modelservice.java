package com.nexsoft.app.service;


import java.util.List;

import javax.transaction.Transactional;

import com.nexsoft.app.model.Model;
import com.nexsoft.app.repo.Modelrepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Modelservice {
    @Autowired
     private Modelrepository modelrepository;

    @Transactional
	public void save(Model model) {
		modelrepository.save(model);
	}

    @Transactional
	public List<Model> get() {
		return modelrepository.findAll();
	}
    
}
