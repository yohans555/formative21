package com.nexsoft.app.Controller;

import java.util.ArrayList;
import com.nexsoft.app.model.Model;
import com.nexsoft.app.service.Modelservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Modelcontroller {
    @Autowired
    Modelservice modelservice;


    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value ="/save", method = RequestMethod.POST, consumes = "application/json")
    public void add(@RequestBody Model model ){
        modelservice.save(model);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value ="/api", produces = "application/json")
    public ArrayList<Model> getData(){
        return (ArrayList<Model>) modelservice.get();
    }

}
