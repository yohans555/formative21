import logo from './logo.svg';
import './App.css';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>Hello World</p>
        <h2>{new Date().toDateString()}</h2>
      </header>
    </div>
  );
}

export default App;
