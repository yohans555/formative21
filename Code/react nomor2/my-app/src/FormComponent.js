// component
import React from 'react';
import {send} from './EventHandler';
import {send2} from './EventHandler'
import ReactDOM from 'react-dom';

class FormComponent extends React.Component{

    render(){
        return (
            <div class="overlay">
                <div class="con">
                <div class="form">
                <header class="head-form"><h2>Zodiac</h2></header><br></br>
                <input class="form-input" id="name" type="text" placeholder="Name" required/>
                {/* <input id="name" type = "text" placeholder = "Enter name"  */}
                <br />
                <br />
                <input class="form-input" id="date" type = "date" required />
                <br />
                <br />
                <button class="log-in" onClick = {send} >Submit</button>
                </div>
                </div>
            </div>
        );
     }
  }

  export default FormComponent;